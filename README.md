# Heart Porsuit
Projeto de desenvolvimento de Jogo de plataforma 2D para cadeira de Projeeto de Desenvolvimento, realizando-se na  Faculdade de Tecnologia Senac Pelotas (RS), este projeto contará com a orientação do Prof. Me. Gladimir Ceroni Catarino. 

# Introdução
Heart Porsuit será um jogo desenvolvido usando a Linguagem de Programação C# e utilizará de recursos de motorização da ferramenta Unity(Unity Technologies). Ele contará com design em Pixel Art, com gênero Plataforma e tecnologia 2D.

# Requisitos
A engine [Unity](https://unity.com/pt), como outras ferramentas do gênero,necessitam de alguns requisitos de sistema, que varia muito dependendo da complexidade do projeto.
Para mais informações acesse o [site](https://unity3d.com/pt/unity/system-requirements).
NOTA: O Unity está disponível para [Mac OS X e Windows](https://store.unity.com/?_ga=2.126865934.249640677.1553827386-1206357891.1546796747), não sendo possível
assim ele ser utilizado no Linux.
A Unity possui versões gratuítas e pagas, porém a versão gratuita é mais do que suficiente para
o desenvolvimento deste projeto.
O jogo Heart Porsuit está sendo desenvolvido na
versão Unity 2018.3.10.
⚠ ATENÇÃO, as ferramentas, comandos, linguagens  e etc. requerem conhecimentos prévios!

# Clone ou Download
Para efetuar o clone do projeto usando os links disponíveis no repositório
GitLab é preciso possuir o software Git instalado na sua máquina.
Para saber como instalar e usar a ferramenta Git acesse o site do
[Git](https://git-scm.com/doc). Caso já saiba como fazer, use os seguintes comandos:
Com SSH:
$ git@gitlab.com:senac-tcc-2019/ojogo2019.git
ou
Com HTTPS:
$ https://gitlab.com/senac-tcc-2019/ojogo2019.git
ou você pode baixar o projeto (download): zip,
tar.gz,
tar.bz2 e
tar.

# Documentação
Está disponivel na [WIKI](https://gitlab.com/senac-tcc-2019/ojogo2019/wikis/home) deste projeto.

# Licença
Este projeto usufrui da seguinte licença de uso: [MIT](https://opensource.org/licenses/MIT)

# Download do Testável e como instalar
Link de acesso: [https://1drv.ms/u/s!AjWzKhjYvROGhIUPqVg80POJtBIqAQ?e=pbiYK3](https://1drv.ms/u/s!AjWzKhjYvROGhIUPqVg80POJtBIqAQ?e=pbiYK3)

Ao realizar o download, o arquivo TESTÁVEL.rar deve ser descompactado, dentro da pasta com o mesmo nome, deverá ser executado o arquivo executável com nome "Plataforma 2D Base para Hearts porsuit.exe"
